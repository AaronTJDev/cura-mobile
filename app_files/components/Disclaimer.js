import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    StatusBar,
    TouchableOpacity,
    AsyncStorage,
    ScrollView,
    Linking
} from 'react-native';

export default class Disclaimer extends Component {
    constructor(props){
        super(props);
        this.state = {
            disclaimerSigned: false,
            termsViewed:false
        }
    }

    storeData = async (disclaimerState) => {
        try {
            await AsyncStorage.setItem("disclaimerSigned", JSON.stringify(disclaimerState));
            console.log("storage success.");
        } catch (error) {
            // Error saving data
            console.log("storage failed.")
        }
    };

    onPress = () => {
        let disclaimerSigned = true;
        this.setState({disclaimerSigned : true});
        this.storeData(disclaimerSigned);
    };

    viewDisclaimer = () => {
        this.setState({termsViewed:true});
        Linking.openURL('http://curafam.com/terms.html');
    };

    render(){

        let termsViewedButton =
            <TouchableOpacity style={styles.button} onPress={this.onPress}>
                <Text style={{fontSize: 32, color: '#5c5b5b', textAlign:'center',flex:1}}>I Agree</Text>
            </TouchableOpacity>;

        let termsNotViewedButton =
            <TouchableOpacity style={[styles.button, {backgroundColor:'#6B6C6C'}]}>
                <Text style={{fontSize: 32, color: '#5c5b5b', textAlign:'center',flex:1}}>I Agree</Text>
            </TouchableOpacity>;

        let disclaimerSigned = this.state.disclaimerSigned;

        let disclaimer =
            <View style={styles.container}>
                <View style={styles.text_container}>
                    <View style={{flexDirection:'row',flex:.2,justifyContent:'center',top:'25%'}}>
                        <Text style={styles.textHeader}>To continue please review our terms and conditions/disclaimer. By selecting "I Agree" you attest that you have reviewed the terms and conditions and consent to the stipulations provided therein.</Text>
                    </View>
                    <View style={styles.textBody}>
                    <Text style={{color:'#ff590f',textAlign:'center',fontSize:18}} onPress={this.viewDisclaimer}>Terms and Conditions/Disclaimer</Text>
                    </View>
                </View>
                {
                    this.state.termsViewed ?
                        termsViewedButton :
                        termsNotViewedButton
                }
            </View>;

        return (
            disclaimerSigned ? null : disclaimer
        )
    }
}

const styles = {
    container: {
        position:"absolute",
        height:"100%",
        width:"100%",
        backgroundColor:"#000",
        zIndex:2,
        opacity:.9,
        padding:0,
        margin:0,
        flex:1
    },
    text_container: {
        flexDirection:"column",
        flex:.92
    },
    textHeader:{
        flexDirection:'row',
        alignSelf:'center',
        textAlign:'center',
        color:'#fcfafa',
        flex:.8,
        fontSize:14
    },
    textBody:{
        flex:1,
        top:'20%'
    },
    button: {
        borderWidth: 1,
        borderColor: "transparent",
        backgroundColor:"#fcfafa",
        padding:8,
        flex:.08
    },
};