import 'babel-polyfill';
import { AppRegistry } from 'react-native';
import Cura from './app_files/Cura';

AppRegistry.registerComponent('Cura', () => Cura);
