/**
 * Created by aaronjackson on 4/11/18.
 */

import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableHighlight,
    TouchableOpacity,
    TouchableWithoutFeedback,
    Button,
    ScrollView,
    Animated,
    Image,
    Linking
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';


export default class ContentBoxExpanded extends Component {
    constructor(props){
        super(props);
        this.state = {
            data:this.props.data,
            statement:[],
            y: new Animated.Value(-600)
        };
    }

    renderExpand = () => {
        Animated.timing(this.state.y, {toValue: 0, duration:200}).start();
    };

    renderClose = () =>{
        let parent = this;
        Animated.timing(this.state.y, {toValue:-600, duration:200}).start();
        setTimeout(function () {
            parent.props.handlePress();
        }, 200);
    };

    componentDidMount(){
        this.renderExpand();
    }

    render(){
        const keyNutrients = this.props.keyNutrients;
        let benefitRows = [];
        let riskRows = [];
        var references = [];
        keyNutrients.forEach(function (nutrientReport, index) {
            let headerText =
                <View>
                    <Text style={style.searchBenefits}>{nutrientReport.nutrient.name}</Text>
                    <Text style={style.RDA}>{nutrientReport.RDA}%</Text>
                </View>;

            if(nutrientReport.benefits.length > 0) {
                benefitRows[index] = nutrientReport.benefits.map((phrase, idx) => (

                    <View style={style.searchBenefitsView} key={'text' + idx}>
                        {
                            idx === 0 ? headerText : null
                        }

                        <Text style={{fontSize:16}}>{phrase.statement}</Text>
                        <Text style={style.references}>References</Text>
                        <View style={{flexDirection:"row", flex:1}}>
                        {
                            phrase.references.map((reference,index) => (
                                <Text key={"ref_idx_" + index} onPress={() => Linking.openURL(reference)} style={style.links}>[{index + 1}]</Text>
                            ))
                        }
                        </View>
                    </View>
                ));
            }

            if(nutrientReport.toxicity.length > 0) {
                riskRows[index] = nutrientReport.toxicity.map((phrase, idx) => (
                    <View style={style.searchRisksView} key={'text' + idx}>
                        <Text style={style.searchRisks}>{nutrientReport.nutrient.name + " | " + phrase}</Text>
                    </View>
                ));
            }

        });


        if(benefitRows.length > 0) {
            let benefitHeader = <Text key="b_header"
                                      style={{flex:1, color:'#1e384a',padding:12,fontSize:14}}>Benefits</Text>;
            benefitRows.splice(0, 0, benefitHeader);
        }

        if(riskRows.length > 0) {
            let riskHeader = <Text key="r_header" style={{flex:1, color:'#bf0918',padding:12,fontSize:14}}>Risks</Text>;
            riskRows.splice(0, 0, riskHeader);
        }

        return (
            <Animated.View style={[style.mainView, {
                transform: [
                    {translateY: this.state.y}
                ]
            }]}>
                <Text style={style.name} onPress={this.renderClose}>{this.state.data.name}</Text>
                {
                    benefitRows.length > 0 ?
                        benefitRows :
                        null
                }
                {
                    riskRows.length > 0 ?
                        riskRows :
                        null
                }
                <TouchableHighlight underlayColor='white' onPress={this.renderClose}><Icon name="chevron-up" size={24} style={style.pop_out_icon}/></TouchableHighlight>
            </Animated.View>
        )
    }
}


let style = {
    main : {
        position:'relative',
        backgroundColor:"#fcfafa",
        zIndex:1
    },
    mainView:{
        position:'relative',
        flexDirection:'column',
        alignItems:'stretch',
        flex:1,
        backgroundColor:"#fcfafa"
    },
    name: {
        color:'#5c5b5b',
        fontSize:22,
        padding:12,
        flex:1,
        alignSelf:'center'
    },
    searchBenefitsView: {
        flex:1,
        padding:20
    },
    searchBenefits: {
        fontSize:18,
        flex:1,
        color:'#5c5b5b',
        flexDirection:"row"
    },
    RDA:{
        flexDirection:"row",
        flex:1,
        color:"#427aa1"
    },
    searchRisksView: {
        flex:1,
        paddingLeft:20,
        paddingBottom:4
    },
    searchRisks: {
        fontSize:14,
        flex:1,
        color:'#5c5b5b'
    },pop_out_icon:{
        alignSelf:"center",
        color:"#5c5b5b",
        flexDirection:"row",
        flex:1,
        margin:12
    },
    references: {
        fontSize:14,
        padding:8,
        paddingLeft:0,
        color: "#1e384a"
    },
    links :{
        fontSize:12,
        flexDirection:"row",
        flex:.1,
        color:"#427aa1"
    }
};
