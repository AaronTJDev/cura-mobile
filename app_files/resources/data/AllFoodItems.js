/**
 * Created by aaronjackson on 9/19/17.
 */
import FoodItemsOnes from './FoodItems';
import FoodItemsTwo from './FoodItems2';

const FoodItems = FoodItemsOnes.concat(FoodItemsTwo);

export default FoodItems;