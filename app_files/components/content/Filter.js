import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    TouchableHighlight,
    TouchableWithoutFeedback,
    Button,
    Animated
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';


export default class Filter extends Component{
    constructor(props){
        super(props);
        this.state = {
            filter: '',
            filterVisible: false,
            itemCountVisible: false,
            x: new Animated.Value(-400)
        }
    }

    renderPopOut = () => {
        let parent = this;
        this.setState({ filterVisible: true  });
        Animated.spring(this.state.x, {toValue: 0, duration:300}).start();

        if(this.state.filterVisible){
            Animated.spring(this.state.x, {toValue: -400, duration:200}).start();
            setTimeout(function () {
                parent.setState({filterVisible: false});
            }, 300);
        }
    };

    filterContent = (content) => {
        let parent = this;
        setTimeout(function () {

            parent.setState({filterVisible: false});
        }, 100);
        this.props.getFilter(content);
    };

    render(){
        if(this.state.filterVisible){
            styles.pop_out.display = 'flex';
        } else {
            styles.pop_out.display = 'none';
        }

        const filterMenu =
            <Animated.View style={[styles.pop_out, {
                transform: [
                    {translateX: this.state.x}
                ]
            }]}>
                <ScrollView style={styles.pop_out_interior}>
                    <TouchableHighlight underlayColor='white' style={styles.filterBox} onPress={() => this.filterContent(this.refs.filterTexta.props.children)}><Text ref="filterTexta" style={styles.filterText}>Dairy and Egg Products</Text></TouchableHighlight>
                    <TouchableHighlight underlayColor='white' style={styles.filterBox} onPress={() => this.filterContent(this.refs.filterTextb.props.children)}><Text ref="filterTextb" style={styles.filterText}>Spices and Herbs</Text></TouchableHighlight>
                    <TouchableHighlight underlayColor='white' style={styles.filterBox} onPress={() => this.filterContent(this.refs.filterTextc.props.children)}><Text ref="filterTextc" style={styles.filterText}>Baby Foods</Text></TouchableHighlight>
                    <TouchableHighlight underlayColor='white' style={styles.filterBox} onPress={() => this.filterContent(this.refs.filterTextd.props.children)}><Text ref="filterTextd" style={styles.filterText}>Fats and Oils</Text></TouchableHighlight>
                    <TouchableHighlight underlayColor='white' style={styles.filterBox} onPress={() => this.filterContent(this.refs.filterTexte.props.children)}><Text ref="filterTexte" style={styles.filterText}>Soups, Sauces, and Gravies</Text></TouchableHighlight>
                    <TouchableHighlight underlayColor='white' style={styles.filterBox} onPress={() => this.filterContent(this.refs.filterTextf.props.children)}><Text ref="filterTextf" style={styles.filterText}>Sausages and Lunch Meats</Text></TouchableHighlight>
                    <TouchableHighlight underlayColor='white' style={styles.filterBox} onPress={() => this.filterContent(this.refs.filterTextg.props.children)}><Text ref="filterTextg" style={styles.filterText}>Breakfast Cereals</Text></TouchableHighlight>
                    <TouchableHighlight underlayColor='white' style={styles.filterBox} onPress={() => this.filterContent(this.refs.filterTexth.props.children)}><Text ref="filterTexth" style={styles.filterText}>Snacks</Text></TouchableHighlight>
                    <TouchableHighlight underlayColor='white' style={styles.filterBox} onPress={() => this.filterContent(this.refs.filterTexti.props.children)}><Text ref="filterTexti" style={styles.filterText}>Fruits and Fruit Juices</Text></TouchableHighlight>
                    <TouchableHighlight underlayColor='white' style={styles.filterBox} onPress={() => this.filterContent(this.refs.filterTextj.props.children)}><Text ref="filterTextj" style={styles.filterText}>Vegetables</Text></TouchableHighlight>
                    <TouchableHighlight underlayColor='white' style={styles.filterBox} onPress={() => this.filterContent(this.refs.filterTextk.props.children)}><Text ref="filterTextk" style={styles.filterText}>Pork</Text></TouchableHighlight>
                    <TouchableHighlight underlayColor='white' style={styles.filterBox} onPress={() => this.filterContent(this.refs.filterTextl.props.children)}><Text ref="filterTextl" style={styles.filterText}>Poultry</Text></TouchableHighlight>
                    <TouchableHighlight underlayColor='white' style={styles.filterBox} onPress={() => this.filterContent(this.refs.filterTextm.props.children)}><Text ref="filterTextm" style={styles.filterText}>Beef</Text></TouchableHighlight>
                    <TouchableHighlight underlayColor='white' style={styles.filterBox} onPress={() => this.filterContent(this.refs.filterTextn.props.children)}><Text ref="filterTextn" style={styles.filterText}>Fish and Shellfish</Text></TouchableHighlight>
                    <TouchableHighlight underlayColor='white' style={styles.filterBox} onPress={() => this.filterContent(this.refs.filterTexto.props.children)}><Text ref="filterTexto" style={styles.filterText}>Beans</Text></TouchableHighlight>
                    <TouchableHighlight underlayColor='white' style={styles.filterBox} onPress={() => this.filterContent(this.refs.filterTextp.props.children)}><Text ref="filterTextp" style={styles.filterText}>Lamb, Veal, and Game</Text></TouchableHighlight>
                    <TouchableHighlight underlayColor='white' style={styles.filterBox} onPress={() => this.filterContent(this.refs.filterTextq.props.children)}><Text ref="filterTextq" style={styles.filterText}>Baked</Text></TouchableHighlight>
                    <TouchableHighlight underlayColor='white' style={styles.filterBox} onPress={() => this.filterContent(this.refs.filterTextr.props.children)}><Text ref="filterTextr" style={styles.filterText}>Sweets</Text></TouchableHighlight>
                    <TouchableHighlight underlayColor='white' style={styles.filterBox} onPress={() => this.filterContent(this.refs.filterTexts.props.children)}><Text ref="filterTexts" style={styles.filterText}>Grains and Pasta</Text></TouchableHighlight>
                    <TouchableHighlight underlayColor='white' style={styles.filterBox} onPress={() => this.filterContent(this.refs.filterTextt.props.children)}><Text ref="filterTextt" style={styles.filterText}>Legumes and Legume Products</Text></TouchableHighlight>
                    <TouchableHighlight underlayColor='white' onPress={this.renderPopOut}><Icon name="chevron-up" size={24} style={styles.pop_out_icon}/></TouchableHighlight>
                </ScrollView>
            </Animated.View>;


        return(
            <View style={{flex:.06, flexDirection:'column',zIndex:5}}>
                <View style={{flex:1,backgroundColor:'#1E384A'}}>
                    <TouchableHighlight onPress={this.renderPopOut} style={styles.filterTab}><Icon size={20} name="sliders" style={styles.tabText}></Icon></TouchableHighlight>
                </View>
                {
                    filterMenu
                }
            </View>
        )
    }
}

let styles = {
    filterTab:{
        flex:1,
        alignItems:'center',

    },
    tabText:{
        color:'#fcfafa',
        margin:4
    },
    pop_out: {
        display:'flex',
        position:'relative',
        backgroundColor: "#1E384A",
        alignItems: "center",
        zIndex:1,
        flex:1
    },
    pop_out_interior:{
        flex:1,
    },
    pop_out_icon:{
        alignSelf:"center",
        color:"#fcfafa",
        margin:8
    },
    filterBox:{
        borderColor:'#fcfafa',
        borderWidth:.5,
        margin:8
    },
    filterText:{
        color:'#fcfafa',
        padding:4,
        alignSelf:'center'
    }
};