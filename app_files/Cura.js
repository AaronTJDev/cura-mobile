/**
 * Created by aaronjackson on 1/16/18.
 */

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    StatusBar,
    Alert,
    AsyncStorage
} from 'react-native';
import { NativeRouter, Route, Switch } from 'react-router-native';
import SplashScreen from 'react-native-splash-screen';
import Disclaimer from './components/Disclaimer';
import Menu from './components/menu/Menu';
import Home from './components/Home';


export default class Cura extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            disclaimerSigned:false
        }
    }
    componentDidMount(){
        SplashScreen.hide();
        this.retrieveData()
    }

    retrieveData = async () => {
        try {
            const value = await AsyncStorage.getItem("disclaimerSigned")
                .then((result) => this.setState({disclaimerSigned: result}));
        } catch (error) {
            // Error retrieving data
            console.log("nothing returned");
        }
    };

    render(){
        return(
            <View style={{flex:1}}>
            <NativeRouter style={{flex:1}}>
                <View style={style.main}>
                    {
                        this.state.disclaimerSigned ?
                            null : <Disclaimer/>
                    }
                    <StatusBar hidden={true}></StatusBar>
                    <Switch style={{flex:1}}>
                        <Route style={{flex:1}} path="/" render={() => (<Home/>)}/>
                    </Switch>
                </View>
            </NativeRouter>
            </View>
        )
    }
}

var style = {
    main: {
        flex:1,
        backgroundColor:'#c4c5c6'
    }
};