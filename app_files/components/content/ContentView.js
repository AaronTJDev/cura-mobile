import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableHighlight,
    TouchableWithoutFeedback,
    Button,
    ScrollView
} from 'react-native';
import SearchBar from '../search_bar/SearchBar';
import ContentBox from './ContentBox';
import Filter from './Filter';

export default class ContentView extends Component{
    constructor(props){
        super(props);
        this.state = {
            hasSearched : this.props.hasSearched,
            content: [],
            filter: undefined
        }
    }

    renderContent = (data, filter) => {
        this.state.content.length = 0;
        let parent = this;

        if(filter) {
            data.forEach(function (food, index) {
                let keyNutrients = food.keyNutrients;
                //CREATE FUNCTION TO COUNT BENEFITS AND DISPLAY TOTAL NUMBER OF BENEFITS AN ITEM HAS

                let benefitsCount = 0;
                let toxicityCount = 0;

                for(let i = 0; i < keyNutrients.length; i++){
                    benefitsCount += keyNutrients[i].benefits.length;
                    toxicityCount += keyNutrients[i].toxicity.length;
                }

                if(food.foodGroup.includes(filter) && (benefitsCount > 0 || toxicityCount > 0)) {
                    parent.state.content.push(<ContentBox key={index} data={food}/>);
                }
            });
        } else {
            data.forEach(function (food, index) {
                let keyNutrients = food.keyNutrients;
                //CREATE FUNCTION TO COUNT BENEFITS AND DISPLAY TOTAL NUMBER OF BENEFITS AN ITEM HAS

                let benefitsCount = 0;
                let toxicityCount = 0;

                for(let i = 0; i < keyNutrients.length; i++){
                    benefitsCount += keyNutrients[i].benefits.length;
                    toxicityCount += keyNutrients[i].toxicity.length;
                }

                if(benefitsCount > 0 || toxicityCount > 0) {
                    parent.state.content.push(<ContentBox key={index} data={food} NBR={food.keyNutrients}/>);
                }
            });
        }
        console.log("content rendered");
    };

    getFilter = (filter) => {
        console.log("filter got");
        this.setState({filter : filter});
        this.renderContent(this.props.data, filter);
    };

    componentWillReceiveProps(nextProps){
        if(this.props != nextProps){
            this.renderContent(nextProps.data);
        }
        console.log("components received props");
    }


    render(){
        let rows = [];
        for(let i = 0; i<25; i++){
            rows.push(this.state.content[i]);
        }

        var noResults = <View style={{flexDirection:"row",flex:1,alignItems:"center",justifyContent:"center"}}><Text style={styles.noResults}>There are no results to display</Text></View>;

        var result;

        if(this.state.content.length > 0){
            result = rows;
        } else if (this.props.hasSearched && this.state.content.length < 1){
            result = noResults;
        } else {
            result = null;
        }

        return (
                <ScrollView style={styles.view}>

                    <Filter getFilter={this.getFilter}/>
                    {
                        result
                    }

                </ScrollView>
        )
    }
}

let styles = {
    view:{
        backgroundColor:'#E6E4E4',
        marginTop:72,
        flex:1,
        flexDirection:'column'
    },
    noResults:{
        textAlign:"center",
        color:'#5c5b5b',
        fontSize:20,
        marginTop:16
    }
};



