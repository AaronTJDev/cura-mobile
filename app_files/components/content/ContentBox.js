import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableHighlight,
    TouchableWithoutFeedback,
    Button
} from 'react-native';
import ContentBoxExpanded from './ContentBoxExpanded';

export default class ContentBox extends Component{
    constructor(props){
        super(props);
        this.state ={
            expanded : false,
            data: this.props.data
        }
    }

    handlePress = () => {
        this.setState({expanded : !this.state.expanded});
    };

    render(){

        let keyNutrients = this.props.data.keyNutrients;
        //CREATE FUNCTION TO COUNT BENEFITS AND DISPLAY TOTAL NUMBER OF BENEFITS AN ITEM HAS

        let benefitsCount = 0;
        let toxicityCount = 0;

        for(let i = 0; i < keyNutrients.length; i++){
            benefitsCount += keyNutrients[i].benefits.length;
            toxicityCount += keyNutrients[i].toxicity.length;
        }

        let name = this.props.data.name;

        contentBox =
            <View style={styles.box}>
            <TouchableHighlight onPress={this.handlePress} underlayColor={"#DCE6ED"}>
                <View>
                    <View style={styles.textView}>
                        <Text style={styles.headerBoxText}>{name}</Text>
                    </View>
                    <View style={styles.textView}>
                        <Text style={styles.benefitText}>{benefitsCount} benefits</Text>
                        <Text style={styles.toxicityText}>{toxicityCount} risks</Text>
                    </View>
                </View>
            </TouchableHighlight>
        </View>;

        return(
            !this.state.expanded ?
                contentBox :
                <ContentBoxExpanded data={this.state.data} handlePress={this.handlePress} keyNutrients={keyNutrients}/>
        )
    }
}

var styles = {
    box:{
        flex:.25,
        backgroundColor:'#fcfafa',
        borderTopColor:'#E6E4E4',
        borderTopWidth:1,
        zIndex:4
    },
    textView:{
        padding:12,
        flex:1,
        flexDirection:'row'
    },
    headerBoxText: {
        color:'#5c5b5b',
        fontSize:18
    },
    benefitText: {
        color:'#427AA1',
        padding:8,
        flex:.5
    },
    toxicityText: {
        color:'#C4C5C6',
        padding:8,
        flex:.5
    }
};