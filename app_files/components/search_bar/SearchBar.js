/**
 * Created by aaronjackson on 1/29/18.
 */
import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    TouchableHighlight,
    TouchableWithoutFeedback,
    Button,
    Animated,
    TextInput
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class SearchBar extends Component{
    constructor(props){
        super(props);
        this.state = {
            text: ''
        };
    }

    render(){
        return(
            <View style={styles.searchBar}>
                    <TextInput
                        style={styles.searchInput}
                        onChangeText={(text) => this.props.handleQuery(text)}
                        value={this.props.text}
                        underlineColorAndroid={'transparent'}
                    />
                    <TouchableHighlight onPress={this.props.handlePress} style={styles.searchButton}>
                        <Text style={styles.buttonText}>Search</Text>
                    </TouchableHighlight>
            </View>
        )
    }
}

let styles = {
    searchBar:{
        flexDirection:'row',
        padding:0,
        height:40,
        zIndex:-1,
        alignSelf:'center',
        marginTop:48
    },
    searchButton: {
        backgroundColor:'#1E384A',
        flex:.2,
        marginLeft:-20
    },
    buttonText: {
        color:'#ebebeb',
        alignSelf:'center',
        padding:10
    },
    searchInput: {
        padding:12,
        backgroundColor:'#fcfafa',
        flex:.5,
        borderRadius:4

    }
};