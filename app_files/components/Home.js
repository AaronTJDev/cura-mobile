/**
 * Created by aaronjackson on 1/29/18.
 */
/*Search View*/
import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    TouchableHighlight,
    TouchableWithoutFeedback,
    Image,
    Button,
    Animated,
    ActivityIndicator
} from 'react-native';
import SearchBar from './search_bar/SearchBar';
import Menu from './menu/Menu';
import Toggle from './Toggle';
import ContentView from './content/ContentView';
import Nutrients from '../resources/data/Nutrients';
import FoodItems from '../resources/data/AllFoodItems';
import Synonyms from '../resources/data/Synonyms';
import removeWords from '../../node_modules/remove-words/index.js';


export default class Home extends Component{
    constructor(props){
        super(props);
        this.state = {
            query:'',
            nutrientData: undefined,
            searchData: [],
            hasSearched:false
        };
        this.child = React.createRef();
    }

    handleQuery = (text) => {
        this.setState({query : text});
    };

    handlePress = () => {
        let searchData = [];
        let nutrientData = this.searchNutrientBenefits(this.state.query);
        searchData = this.findFoodItemsWithRelevantNutrients(nutrientData);
        this.setState({searchData : searchData});
        this.setState({hasSearched: true});
    };

    findFoodItemsWithRelevantNutrients = (relevantNutrients) => {
        let foodItems = [];

        for(let q=0; q<FoodItems.length; q++) {
            let food = FoodItems[q];
            //Goes through 20 foods present in FoodItems array
            let foodProps = Object.entries(food); //breaks each field for a given food object into key/value pairs
            food.relevanceScore = 0;
            food.keyNutrients = [];

            for (let x=0; x < relevantNutrients.length; x++){
                var nutrientReport = relevantNutrients[x]; //deep clones the nutrient report
                let currentNutrient = nutrientReport.nutrient;

                for(let j=0; j<foodProps.length; j++) {
                    let prop = foodProps[j];
                    let propName = prop[0];
                    let propValue = prop[1];
                    //see if the nutrient is present in a significant amount in the food
                    if( propName === currentNutrient.name && propValue > 33 ) {
                        nutrientReport = Object.assign({}, relevantNutrients[x]); //deep clones the nutrient report
                        nutrientReport.RDA = propValue;
                        food.relevanceScore += nutrientReport.RDA < 99 ? nutrientReport.RDA : 0;
                        food.relevanceScore = nutrientReport.toxicity.length < 1 ? food.relevanceScore + 200 : 0;
                        food.keyNutrients.push(nutrientReport);

                    }
                }
            }

            if(food.relevanceScore > 25) {
                foodItems.push(food);
            }
        }

        foodItems.sort(function (foodA, foodB) {
            return foodB.relevanceScore - foodA.relevanceScore;
        });
        return foodItems;
    };

    searchNutrientBenefits = query =>{
        query = query.toLowerCase();
        var initialQuery;
        let queries = [];
        let queryFragments = query.split("+");

        let words = ["have", "not"];

        initialQuery = queryFragments.join(' ');
        let removedPronounsArr = removeWords(initialQuery, true);
        let removedPronounStr = removedPronounsArr.join(' ');
        let synonyms = Synonyms.findSynonym(initialQuery);
        let removedPronounSynonym = Synonyms.findSynonym(removedPronounStr);
        queries = queries.concat(initialQuery);
        queries = queries.concat(synonyms);
        queries = queries.concat(removedPronounStr);
        queries = queries.concat(removedPronounSynonym);

        //deletes duplicates in array
        queries = queries.filter(function (elem,index,self) {
            return index === self.indexOf(elem);
        });


        let nutrientArr = [];

        queries.forEach(function (word) {

            Nutrients.forEach(function (nutrient) {

                let nutrientBody={};
                let benefitArr = [];
                let toxicArr = [];
                let deficiencyArr = [];

                nutrient.benefitStatements.forEach(function (benefit) {
                    benefit.statement = benefit.statement.toLowerCase();
                    if(benefit.statement.includes(word)){
                        benefitArr.push(benefit);
                    }
                });

                nutrient.toxicitySymptoms.forEach(function (toxicity) {
                    toxicity = toxicity.toLowerCase();
                    if(toxicity.includes(word)){
                        toxicArr.push(toxicity);
                    }
                });

                nutrient.deficiencySymptoms.forEach(function (deficiency) {
                    deficiency = deficiency.toLowerCase();
                    if(deficiency.includes(word)){
                        deficiencyArr.push(deficiency);
                    }
                });

                let hasBenefits = benefitArr.length > 0 ? true : false;
                let hasToxics = toxicArr.length > 0 ? true : false;
                let hasDeficiencies = deficiencyArr.length > 0 ? true : false;

                nutrientBody = {
                    nutrient: nutrient,
                    benefits: benefitArr,
                    toxicity: toxicArr,
                    deficiencyArr: deficiencyArr,
                    relevance: benefitArr.length + toxicArr.length + deficiencyArr.length,
                    RDA : 0
                };

                if(hasBenefits || hasToxics || hasDeficiencies){
                    nutrientArr.push(nutrientBody);
                }
            });
        });


        return nutrientArr;
    };

    getNutrientOrganBenefitScores(nutrient) {
        //Go through each benefit score of a given nutrient, and return the values in an array
        let NutrientOrganBenefitScores = [["bsCirculatory", 0], ["bsRespiratory", 0], ["bsNervous", 0], ["bsIntegumentary", 0], ["bsMuscular", 0], ["bsSkeletal", 0], ["bsImmune", 0], ["bsEndocrine", 0], ["bsDigestive", 0], ["bsReproductive", 0]];
        let NutrientItemProperties = Object.getOwnPropertyNames(nutrient);

        for (let x = 0; x < NutrientOrganBenefitScores.length; x++) {
            let currentOrganBenefitName = NutrientOrganBenefitScores[x][0];
            for (let i = 0; i < NutrientItemProperties.length; i++) {
                let currentNutrientItemProperty = NutrientItemProperties[i];
                if (currentNutrientItemProperty.includes(currentOrganBenefitName)) {
                    let organBenefitScore = Reflect.get(nutrient, currentNutrientItemProperty);
                    NutrientOrganBenefitScores[x][1] = organBenefitScore;
                }
            }
        }
        return NutrientOrganBenefitScores;
    }

    //Tags are assigned to food items during this function aswell
    getNutritionalFoodItemsKeyValuePairs(foodItem, nutrients){
        let nutrient = nutrients;
        let foodKeyValues = this.getAllFoodItemKeyValuePairs(foodItem);
        let foodItemNutrients = [];
        for(let i=0; i<foodKeyValues.length; i++){
            for(let j=0; j<nutrients.length; j++){
                if(foodKeyValues[i][0] === nutrient[j].name){
                    foodItemNutrients.push([nutrient[j],foodKeyValues[i][1]]);
                    foodItem.tags.push(nutrients[j].name);
                }
            }
        }
        return foodItemNutrients;
    }

    getAllFoodItemKeyValuePairs(foodItem){
        let foodItemProperties = [];
        let foodItemKeyValuePairs = [];
        foodItemProperties = Object.getOwnPropertyNames(foodItem);
        for(let i=0;i<foodItemProperties.length; i++){
            let value = Reflect.get(foodItem, foodItemProperties[i]);
            foodItemKeyValuePairs.push([foodItemProperties[i], value]);
        }
        return foodItemKeyValuePairs;
    }
    //returns a food item with its totalOrganBenefitScores array fully populated.
    getItemTotalOrganBenefitScores(foodItem){
        foodItem.organBenefitScores = [];
        let foodItemNutrients = this.getNutritionalFoodItemsKeyValuePairs(foodItem, Nutrients);
        let foodItemsTotalOrganBenefitScore = [["bsCirculatory", 0], ["bsRespiratory", 0], ["bsNervous", 0], ["bsIntegumentary", 0], ["bsMuscular", 0], ["bsSkeletal", 0], ["bsImmune", 0], ["bsEndocrine", 0], ["bsDigestive", 0], ["bsReproductive", 0]];;
        for(let i=0;i<foodItemNutrients.length;i++){
            let currentNutrient = foodItemNutrients[i][0];
            let nutrientRDA = foodItemNutrients[i][1];
            if(nutrientRDA >= 25){
                let currentNutrientBS = this.getNutrientOrganBenefitScores(currentNutrient);
                for(let x=0; x<foodItemsTotalOrganBenefitScore.length;x++){
                    foodItemsTotalOrganBenefitScore[x][1] += currentNutrientBS[x][1];
                }
            }
        }
        foodItem.organBenefitScores.push(foodItemsTotalOrganBenefitScore);
        return foodItem;
    }


    render(){
        return(
            <View style={{flex:1}}>
                <Menu/>
                <SearchBar handlePress={this.handlePress} handleQuery={this.handleQuery} query={this.state.query}/>
                <ContentView data={this.state.searchData} hasSearched={this.state.hasSearched}/>
            </View>
        )
    }
}