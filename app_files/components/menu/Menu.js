/**
 * Created by aaronjackson on 1/16/18.
 */

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    StatusBar,
    TouchableHighlight,
    TouchableWithoutFeedback,
    Image,
    Button,
    Animated,
    Linking
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Link } from 'react-router-native';


export default class Menu extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            menuVisibility: false,
            y: new Animated.Value(-200),
            fadeAnim: new Animated.Value(1)
        };
    }

    renderPopOut = () => {
        let parent = this;
        this.setState({ menuVisibility: true  });
        Animated.timing(this.state.fadeAnim, {toValue: 0, duration: 300}).start();
        Animated.spring(this.state.y, {toValue: 56, duration:300}).start();

        if(this.state.menuVisibility){
            Animated.timing(this.state.fadeAnim, {toValue: 1, duration: 300}).start();
            Animated.spring(this.state.y, {toValue: -200, duration:300}).start();
            setTimeout(function () {
                parent.setState({menuVisibility: false});
            }, 300);
        }
    };

    render(){
        const popOutMenu =
            <Animated.View style={[styles.pop_out, {
                transform: [
                    {translateY: this.state.y}
                ]
            }]}>
                <Text onPress={() => Linking.openURL('http://curafam.com/')} style={styles.menuText}>About</Text>
                <Text onPress={() => Linking.openURL('http://curafam.com/feedback.html')} style={styles.menuText}>Support</Text>
                <Text onPress={() => Linking.openURL('http://curafam.com/terms.html')} style={styles.menuText}>T&C</Text>
                <TouchableHighlight onPress={this.renderPopOut}><Icon name="chevron-up" size={20} style={styles.pop_out_icon}/></TouchableHighlight>
            </Animated.View>;

        return(
            <View>
                <View style={styles.menu}>
                    <TouchableHighlight style={styles.menuImg}>
                        <Image style={{height:48, width:84}} source={require('../../resources/cura.png')} />
                    </TouchableHighlight>
                    <Animated.View style={[styles.menuItems, { opacity:this.state.fadeAnim }]}>
                        <TouchableWithoutFeedback onPress={this.renderPopOut}>
                            <Icon name="bars" size={24} style={styles.menuItem}/>
                        </TouchableWithoutFeedback>
                    </Animated.View>

                </View>

                {
                    this.state.menuVisibility ?
                        popOutMenu : null
                }
            </View>
        )
    }
}

var styles = {
    menu: {
        backgroundColor:'#e6e4e4',
        flexDirection:'row',
    },
    menuImg:{
        alignSelf:"flex-start",
        marginTop:8,
        marginLeft:8,
        flex:.5
    },
    menuItems:{
        flex:.5,
        alignSelf:"center",
        alignItems:"flex-end"
    },
    menuItem:{
        marginRight:16,
        color:"#C4C5C6"
    },
    pop_out: {
        alignSelf:'flex-end',
        width:100,
        position:'absolute',
        backgroundColor: "#171717",
        alignItems: "center",
        opacity: 0.98,
        zIndex:1
    },
    menuText:{
        margin:12,
        color:"#E6E4E4",
        fontSize:14
    },
    pop_out_icon:{
        alignSelf:"center",
        color:"#5C5B5B",
        marginBottom:8
    }
};



