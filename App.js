import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    StatusBar,
    TextInput,
    Button,
    Alert,
    TouchableHighlight
} from 'react-native';


export default class App extends Component<> {

    _onPressButton() {
        Alert.alert('You tapped the button!')
      }

    render(){
        return(
            <View style={styles.app}>
                <TextInput style={{flex:.25, backgroundColor: '#fff'}} placeholder="Type here." onChangeText={(text) => this.setState({text})} />
                <StatusBar hidden={true}></StatusBar>
                <View style={styles.box}>
                    <Text>This is text in a box</Text>
                </View>
                <View style={styles.box}>
                    <Text>This is text too.</Text>
                </View>
                <View style={styles.box}>
                    <Text>This is text also.</Text>
                </View>
                <Button
                    style={{flex:1}}
                    onPress={() => {
                        Alert.alert('You tapped the button!');
                    }}
                    title="Press Me"
                />
                <TouchableHighlight underlayColor="white" onPress={this._onPressButton}>
                    <View style={styles.button}>
                        <Text style={styles.buttonText}>Button</Text>
                    </View>
                </TouchableHighlight>
            </View>


        );
    }
}

var styles = StyleSheet.create({
    app: {
        flex:1,
        flexDirection: 'column',
    },
    box: {
        borderColor: 'red',
        backgroundColor: '#fff',
        borderWidth: 1,
        padding: 10,
        flex:.2,
        height:40
    },
    button: {
        marginBottom: 30,
        width: 260,
        alignItems: 'center',
        backgroundColor: '#2196F3'
      },
      buttonText: {
        padding: 20,
        color: 'white'
      }
});
