/**
 * Created by aaronjackson on 9/26/17.
 */
const findSynonym = (string) => {
    let Symptoms =[
        {
            name: 'headache',
            synonyms: ["migraine", "migraines", "head throbbing", "splitting headache","head hurts", "head ache"]
        },
        {
            name: 'muscle pain',
            synonyms: ["sore arm", "sore leg", "arm pain", "leg pain", "back pain", "back hurts", "sore neck", "neck sore", "foot hurts", "ankle hurts", "lower back pain", "shoulder hurt", "toe hurts", "finger hurts","my back hurt","my lower back hurt", "back sore", "arm sore", "arms sore", "arm's sore", "leg sore", "legs sore", "leg's sore", "shoulder sore" ,"shoulder's sore", "shoulders sore", "neck sore", "thigh sore", "thighs sore", "thigh's sore", "calf sore", "calf's sore", "calves sore", "calve's sore", "ankle sore", "ankle's sore", "ankles sore" ]
        },
        {
            name: 'muscular health',
            synonyms: ["sore arm", "sore leg", "arm pain", "leg pain", "back pain", "back hurts", "sore neck", "neck sore", "foot hurts", "ankle hurts", "lower back pain", "shoulder hurt", "toe hurts", "finger hurts","stomach cramps","shoulder","arm","leg","hand","foot","muscle"]
        },
        {
            name: 'digestive',
            synonyms: ["stomach ache","stomach cramps", "stomach pain", "acid reflux", "GERD", "bad gas", "indigestion"]
        },
        {
            name: 'diarrhea',
            synonyms: ["bubble guts", "upset stomach", "diarhea", "runny stool"]
        },
        {
            name: 'constipation',
            synonyms: ["can't poop","difficulty pooping"]
        },
        {
            name: 'diabetes',
            synonyms: ["type ii diabetes", "diabetes mellitus", "type 1 diabetes", "type 2 diabetes", "type i diabetes","high blood sugar"]
        },
        {
            name: 'joint',
            synonyms: ["back pain","ankle hurts", "wrist hurts"]
        },
        {
            name: 'arthritis',
            synonyms: ["joint pain","knee","ankle hurts","neck pain","wrist pain","heel pain","heel hurts"]
        },
        {
            name: 'weight',
            synonyms: ["obesity","weight loss","overweight","fat loss"]
        },
        {
            name: 'allergies',
            synonyms: ["sneezing", "runny nose", "hay fever", "rhinitis", "stuffed nose", "stuffed up nose", "stuffy nose", "nose is stuffy","itchy throat","throat is itchy","eyes keep watering","itchy eyes","eyes are itching"]
        },
        {
            name:'fatigue',
            synonyms: ["sleepy","tired","drowsy","drowsiness","tiredness","sleepiness","worn out"]
        },
        {
            name:'immune',
            synonyms : ["immunodeficiency", "immune disorder"]
        },
        {
            name:'relax',
            synonyms : ["stiff", "stiffness", "rigid", "flexible", "flexibility", "back stiff", "muscle stiff", "muscles stiff", "muscle's stiff","stiff arm", "stiff leg", "stiff neck", "neck stiff", "foot hurts", "back stiff", "arm stiff", "arm's stiff", "arms stiff", "leg stiff", "legs stiff", "leg's stiff", "shoulder stiff" ,"shoulder's stiff", "shoulders stiff", "neck stiff", "thigh stiff", "thighs stiff", "thigh's stiff", "calf stiff", "calf's stiff", "calves stiff", "calve's stiff", "ankle stiff", "ankle's stiff", "ankles stiff"]
        },
        {
            name:'heart',
            synonyms : ["heart attack", "chest pain", "chest"]
        }
    ];

    Symptoms.forEach(function (symptom) {
        if(string === symptom.name) {
            return false
        } else {
            symptom.synonyms.forEach(function (synonym) {
                if(synonym.length > 4 && string === synonym){
                    string = symptom.name;
                }
            });
        }
    });
    if(string) {
        return string;
    }
};

const Synonyms = { findSynonym };
export default Synonyms;