/**
 * Created by aaronjackson on 6/11/18.
 */
import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableHighlight,
    TouchableOpacity,
    TouchableWithoutFeedback,
    Button,
    ScrollView,
    Animated,
    Image
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class Toggle extends Component {

    render(){
        return(
            <View style={{flex:.05}}>
                <View style={style.buttons_view}>
                    <View style={style.button_view}>
                        <TouchableHighlight style={style.button_text}>
                            <Text style={{color:"#fcfafa"}}>Symptom</Text>
                        </TouchableHighlight>
                    </View>
                    <View style={style.button_view_2}>
                        <TouchableHighlight style={style.button_text}>
                            <Text style={{color:"#fcfafa"}}>Food</Text>
                        </TouchableHighlight>
                    </View>
                </View>
            </View>
        )
    }
}

let style = {
    header_text_view:{
        flex:1
    },
    header_text : {
        flex: 1,
        textAlign:"center"
    },
    buttons_view : {
        flex:1,
        flexDirection:"row",
        backgroundColor:"#5c5b5b"
    },
    button_view_2 : {
        flex:1,
        backgroundColor:"#5c5b5b"
    },
    button_view : {
        flex:1,
        backgroundColor:"#5c5b5b",
        borderRightWidth:1,
        borderColor:"#ecebeb"
    },
    button_text : {
        flex:1,
        alignItems:"center"
    }
};